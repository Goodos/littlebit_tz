﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class UIController : MonoBehaviour
{
    [SerializeField] Text _currLvlText;
    [SerializeField] Text _nextLvlText;
    [SerializeField] GameObject _gameoverPanel;
    private void Start()
    {
        GameController.singleton.GameoverAction += ShowGameOverPanel;
    }

    void Update()
    {
        _currLvlText.text = "Current lvl: " + (GameController.singleton.CurrLvl).ToString();
        _nextLvlText.text = "Next lvl: " + (GameController.singleton.CurrLvl + 1).ToString();
        if (GameController.singleton.CurrLvl >= 3)
        {
            _currLvlText.text = "Current lvl: none";
            _nextLvlText.text = "Next lvl: none";
        }
    }

    void ShowGameOverPanel()
    {
        Instantiate(_gameoverPanel, transform);
    }
}
