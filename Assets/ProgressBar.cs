﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class ProgressBar : MonoBehaviour
{
    [SerializeField] private Image _bar;

    void Update()
    {
        float percent = (float)GameController.singleton.CurrObjectsNumber / (float)GameController.singleton.ObjectsNumber;
        _bar.fillAmount = percent;
    }
}
