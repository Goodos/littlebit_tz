﻿using System;
using System.Collections;
using UnityEngine;
using UnityEngine.Events;

public class GameController : MonoBehaviour
{
    public enum TargetZone
    {
        RedZone,
        GreenZone,
        BlueZone
    }

    public static GameController singleton { get; private set; }

    public UnityAction GameoverAction;
    public GameObject SelectedObject;
    public int ObjectsNumber = 0;
    public int CurrObjectsNumber = 0;
    public int CurrLvl;

    [SerializeField] private LayerMask _ignoreMe;
    [SerializeField] private Data _data;
    [SerializeField] private GameObject _ps;
    

    private void Awake()
    {
        singleton = this;
        CurrLvl = 1;
    }

    void Start()
    {
        _ignoreMe = 1 << 8;
        _ignoreMe = ~_ignoreMe;
        _data.Spawn(CurrLvl);
        ObjectsNumber = _data.ObjectsNumber;
    }

    void Update()
    {
        if (Input.GetMouseButtonDown(0))
        {
            ClickToSelect();
        }
        if (CurrObjectsNumber == ObjectsNumber)
        {            
            ChangeLvl(() =>
            {
                GameoverAction?.Invoke();
            });
        }
    }

    void ClickToSelect()
    {
        RaycastHit hit;
        if (!Physics.Raycast(Camera.main.ScreenPointToRay(Input.mousePosition), out hit, 100, _ignoreMe))
            return;
        if (!hit.transform)
            return;
        if (hit.transform.CompareTag("Box") || hit.transform.CompareTag("Character"))
        {
            SelectedObject = hit.collider.gameObject;
        }
    }

    void ChangeLvl(Action endGame = null)
    {
        _data.ClearGameField();
        CurrLvl++;
        CurrObjectsNumber = 0;
        if (CurrLvl == 2)
        {            
            StartCoroutine(Fireworks(() =>
            {
                _data.Spawn(CurrLvl);
                return;
            }));            
        }
        if (CurrLvl == 3)
        {
            StartCoroutine(Fireworks(() =>
            {                
                endGame?.Invoke();
            }));
        }
    }

    IEnumerator Fireworks(Action endFireworks = null)
    {
        _ps.SetActive(true);
        yield return new WaitForSeconds(2f);
        _ps.SetActive(false);
        endFireworks?.Invoke();
    }
}
