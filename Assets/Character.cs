﻿using UnityEngine;
using UnityEngine.AI;

public class Character : MonoBehaviour
{
    private Outline _outline;
    private NavMeshAgent _agent;
    private Material _blinkMat;
    private Color _blinkColor;
    private float _blinkAmount;
    [SerializeField] private GameController.TargetZone targetZone;

    public void Init(Vector3 startPos, int zone)
    {
        transform.position = startPos;
        targetZone = (GameController.TargetZone)zone;
    }

    private void OnTriggerEnter(Collider other)
    {
        if (other.CompareTag(targetZone.ToString()))
        {
            if (GameController.singleton.SelectedObject == gameObject)
            {
                GameController.singleton.SelectedObject = null;
            }
            _blinkMat.SetColor("_TintColor", Color.white);
            gameObject.layer = 8;
            _agent.isStopped = true;
            GetComponent<Animator>().SetBool("toRun", false);
            GameController.singleton.CurrObjectsNumber++;
        }
    }

    void Start()
    {
        _outline = GetComponent<Outline>();
        _agent = GetComponent<NavMeshAgent>();
        _blinkMat = GetComponent<MeshRenderer>().sharedMaterial;
        switch (targetZone)
        {
            case GameController.TargetZone.RedZone:
                _blinkColor = Color.red;
                break;
            case GameController.TargetZone.GreenZone:
                _blinkColor = Color.green;
                break;
            case GameController.TargetZone.BlueZone:
                _blinkColor = Color.blue;
                break;
        }
        _blinkMat.SetColor("_TintColor", _blinkColor);
    }

    void Update()
    {
        if (GameController.singleton.SelectedObject == gameObject)
        {
            _outline.enabled = true;
        }
        else
        {
            _outline.enabled = false;
        }
        Vector3 diff = _agent.destination - transform.position;
        if (diff.magnitude < .1f)
        {
            GetComponent<Animator>().SetBool("toRun", false);
        }
        if (_blinkAmount >= .1f)
        {
            _blinkAmount = Mathf.Clamp(Mathf.Lerp(_blinkAmount, 0f, Time.deltaTime), 0, 1f);
            _blinkMat.SetFloat("_TintAmount", _blinkAmount);
        }
        else
        {
            _blinkAmount += 1f;
        }
    }
}
