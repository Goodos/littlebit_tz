﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Box : MonoBehaviour
{   
    [SerializeField] private GameController.TargetZone targetZone;
    private MeshRenderer _boxView;
    private Color _boxColor;
    private float _blinkAmount;

    public void Init(Vector3 startPos, int zone)
    {
        transform.position = startPos;
        targetZone = (GameController.TargetZone)zone;
    }

    private void Start()
    {
        _boxView = GetComponent<MeshRenderer>();
        switch (targetZone)
        {
            case GameController.TargetZone.RedZone:
                _boxColor = Color.red;
                break;
            case GameController.TargetZone.GreenZone:
                _boxColor = Color.green;
                break;
            case GameController.TargetZone.BlueZone:
                _boxColor = Color.blue;
                break;
        }
        _boxView.sharedMaterial.SetColor("_TintColor", _boxColor);
    }

    private void OnTriggerEnter(Collider other)
    {
        if (other.CompareTag(targetZone.ToString()))
        {
            if (GameController.singleton.SelectedObject == gameObject)
            {
                GameController.singleton.SelectedObject = null;
            }
            gameObject.layer = 8;
            _boxView.sharedMaterial.SetColor("_TintColor", Color.white);
            GameController.singleton.CurrObjectsNumber++;
        }
    }

    private void Update()
    {
        if (GameController.singleton.SelectedObject == gameObject)
        {
            _boxView.GetComponent<Outline>().enabled = true;
        }
        else
        {
            _boxView.GetComponent<Outline>().enabled = false;
        }
        if (_blinkAmount >= .1f)
        {
            _blinkAmount = Mathf.Clamp(Mathf.Lerp(_blinkAmount, 0f, Time.deltaTime), 0, 1f);
            _boxView.sharedMaterial.SetFloat("_TintAmount", _blinkAmount);
        }
        else
        {
            _blinkAmount += 1f;
        }
    }
}
