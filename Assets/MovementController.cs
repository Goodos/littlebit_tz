﻿using UnityEngine;
using UnityEngine.AI;

public class MovementController : MonoBehaviour
{
    Vector2 startPos;
    float startTime;
    Vector2 swipe;

    void Update()
    {
        if (GameController.singleton.SelectedObject != null)
        {
            if (GameController.singleton.CurrLvl == 1)
            {
                CharMovement();
            }
            if (GameController.singleton.CurrLvl == 2)
            {
                BoxMovement(GameController.singleton.SelectedObject.GetComponent<Rigidbody>());
            }
        }
    }

    void BoxMovement(Rigidbody rb)
    {
        if (Input.GetMouseButtonDown(0))
        {
            startPos = new Vector2(Input.mousePosition.x / (float)Screen.width, Input.mousePosition.y / (float)Screen.width);
            startTime = Time.time;
        }
        if (Input.GetMouseButtonUp(0))
        {
            if (Time.time - startTime > .5f) // press too long
                return;

            Vector2 endPos = new Vector2(Input.mousePosition.x / (float)Screen.width, Input.mousePosition.y / (float)Screen.width);

            swipe = new Vector2(endPos.x - startPos.x, endPos.y - startPos.y);

            if (swipe.magnitude < .17f) // Too short swipe
                return;
            rb.AddForce(new Vector3(swipe.x, 0, swipe.y) * 800f);
        }
    }

    void CharMovement()
    {
        var obj = GameController.singleton.SelectedObject;
        var agent = obj.GetComponent<NavMeshAgent>();
        if (Input.GetMouseButtonDown(0))
        {
            if (obj.CompareTag("Character"))
            {
                RaycastHit hit;
                if (!Physics.Raycast(Camera.main.ScreenPointToRay(Input.mousePosition), out hit))
                    return;
                if (!hit.transform)
                    return;
                if (!hit.transform.CompareTag("Character"))
                {
                    obj.GetComponent<Animator>().SetBool("toRun", true);
                    agent.destination = new Vector3(hit.point.x, 0, hit.point.z);
                }
            }
        }
    }
}
