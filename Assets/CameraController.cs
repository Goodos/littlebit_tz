﻿using UnityEngine;

public class CameraController : MonoBehaviour
{
    void Update()
    {
        if (GameController.singleton.SelectedObject == null)
        {
            if (SwipeInput.swipedLeft)
            {
                transform.RotateAround(Vector3.zero, Vector3.up, -10f);
            }
            if (SwipeInput.swipedRight)
            {
                transform.RotateAround(Vector3.zero, Vector3.up, 10f);
            }
        }        
    }
}
