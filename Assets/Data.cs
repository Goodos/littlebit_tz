﻿using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu]
public class Data : ScriptableObject
{
    [HideInInspector] public int ObjectsNumber = 0;
    [HideInInspector] private List<GameObject> _objects = new List<GameObject>();
    [SerializeField] private GameObject _startPlane;

    [Header("first lvl")]
    [SerializeField] private GameObject[] _characters;

    [Header("second lvl")]
    [SerializeField] private GameObject _box;
    [SerializeField] private Material[] _boxMat;

    public void Spawn(int lvl)
    {
        if (lvl == 1)
        {
            int i = 0;
            foreach (GameObject character in _characters)
            {
                var charPos = RandomPos(new Vector3(character.GetComponent<CapsuleCollider>().radius, 0, character.GetComponent<CapsuleCollider>().radius));
                GameObject newCharacter = Instantiate(character);
                newCharacter.GetComponent<Character>().Init(charPos, i);
                _objects.Add(newCharacter);
                i++;
            }
            ObjectsNumber = i;
        }
        if (lvl == 2)
        {
            int _boxNumbers = 3;
            for (int i = 0; i < _boxNumbers; i++)
            {
                var boxPos = RandomPos(_box.GetComponent<BoxCollider>().size);
                GameObject newBox = Instantiate(_box);
                newBox.GetComponent<MeshRenderer>().sharedMaterial = _boxMat[i];
                newBox.GetComponent<Box>().Init(boxPos, i);
                _objects.Add(newBox);
            }
            ObjectsNumber = _boxNumbers;
        }        
    }

    public void ClearGameField()
    {
        foreach(GameObject gameObject in _objects)
        {
            Destroy(gameObject);
        }
        _objects.Clear();
    }

    Vector3 RandomPos(Vector3 objectSize)
    {
        var bounds = _startPlane.GetComponent<MeshRenderer>().bounds.size;
        var center = _startPlane.GetComponent<MeshRenderer>().bounds.center;
        var x1 = center.x - bounds.x / 2 + objectSize.x;
        var x2 = center.x + bounds.x / 2 - objectSize.x;
        var z1 = center.z - bounds.z / 2 + objectSize.z;
        var z2 = center.z + bounds.z / 2 - objectSize.z;

        var randX = Random.Range(x1, x2);
        var randZ = Random.Range(z1, z2);

        return new Vector3(randX, objectSize.y / 2, randZ);
    }
}
